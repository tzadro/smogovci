﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Models.ExportModels;
using VaterpoloLiga.Models.ImportModels;

namespace VaterpoloLiga.Controllers
{
    public class VirtualniTimoviController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/VirtualniTimovi
        public IQueryable<VirtualniTim> GetVirtualniTimovi()
        {
            return db.VirtualniTimovi;
        }

        // GET: api/VirtualniTimovi/5
        [ResponseType(typeof(VirtualniTim))]
        public IHttpActionResult GetVirtualniTim(int id)
        {
            VirtualniTim virtualniTim = db.VirtualniTimovi.Find(id);
            if (virtualniTim == null)
            {
                return NotFound();
            }

            var igraci = db.Izabrani.Where(s => s.VirtualniTimID == id)
                                    .Select(s => new IgracExport
                                        {
                                            ID = s.Igrac.ID,
                                            Ime = s.Igrac.Ime,
                                            Prezime = s.Igrac.Prezime,
                                            BrojKapice = s.Igrac.BrojKapice,
                                            Vrijednost = s.Igrac.Vrijednost,
                                            TimID = s.Igrac.TimID,
                                            PozicijaID = s.Igrac.PozicijaID,
                                            Tim = s.Igrac.Tim,
                                            Pozicija = s.Igrac.Pozicija
                                        })
                                    .ToList();

            int timSum = 0;
            foreach (IgracExport igrac in igraci) {
                var vrijednosti = db.Dogadaji.Where(s => s.IgracID == igrac.ID)
                                          .Select(s => s.Tip.Vrijednost);

                int igracSum = 0;
                foreach (int vrijednost in vrijednosti)
                {
                    igracSum += vrijednost;
                    timSum += vrijednost;
                }

                igrac.BrojBodova = igracSum;
            }

            var result = new VirtualniTimExport
            {
                ID = virtualniTim.ID,
                Naziv = virtualniTim.Naziv,
                BrojBodova = timSum,
                Igraci = igraci
            };
            return Ok(result);
        }

        // PUT: api/VirtualniTimovi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVirtualniTim(int id, VirtualniTim virtualniTim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != virtualniTim.ID)
            {
                return BadRequest();
            }

            db.Entry(virtualniTim).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VirtualniTimExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VirtualniTimovi
        [ResponseType(typeof(VirtualniTim))]
        public IHttpActionResult PostVirtualniTim(VirtualniTimImport virtualniTim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            VirtualniTim virtualniTimEntry = new VirtualniTim
            {
                Naziv = virtualniTim.Naziv
            };

            db.VirtualniTimovi.Add(virtualniTimEntry);
            db.SaveChanges();

            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Vratar);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Centar);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Bek);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Krilo1);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Krilo2);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Vanjski1);
            InsertIzabrani(virtualniTimEntry.ID, virtualniTim.Vanjski2);

            return Ok(virtualniTimEntry.ID);
        }

        private void InsertIzabrani(int virtualniTimID, int igracID)
        {
            Izabran izabranEntry = new Izabran
            {
                VirtualniTimID = virtualniTimID,
                IgracID = igracID
            };

            db.Izabrani.Add(izabranEntry);
            db.SaveChanges();

            return;
        }

        // DELETE: api/VirtualniTimovi/5
        [ResponseType(typeof(VirtualniTim))]
        public IHttpActionResult DeleteVirtualniTim(int id)
        {
            VirtualniTim virtualniTim = db.VirtualniTimovi.Find(id);
            if (virtualniTim == null)
            {
                return NotFound();
            }

            db.VirtualniTimovi.Remove(virtualniTim);
            db.SaveChanges();

            return Ok(virtualniTim);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VirtualniTimExists(int id)
        {
            return db.VirtualniTimovi.Count(e => e.ID == id) > 0;
        }
    }
}