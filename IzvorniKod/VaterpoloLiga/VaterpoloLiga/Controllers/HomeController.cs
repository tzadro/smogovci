﻿using System.Web.Mvc;

namespace hrcloud_application.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}