﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Models.ExportModels;
using VaterpoloLiga.Models.Tablice;

namespace VaterpoloLiga.Controllers
{
    [RoutePrefix("api/Tablice")]
    public class TabliceController : ApiController
    {
        private DatabaseContext db;
        private UserManager<Korisnik> _userManager;

        public TabliceController()
        {
            db = new DatabaseContext();
            _userManager = new UserManager<Korisnik>(new UserStore<Korisnik>(db));
        }

        [AcceptVerbs("GET")]
        [Route("Globalno")]
        public List<Natjecatelji> Globalno() {
            
            var natjecatelji = _userManager.Users.Where(s => s.OvlastID == 4)
                                                 .Select(s => new Natjecatelji {
                                                        UserName = s.UserName,
                                                        VirtualniTim = db.VirtualniTimovi.Where(e => e.ID == s.VirtualniTimID).FirstOrDefault()
                                                 }).ToList();

            foreach (Natjecatelji natjecatelj in natjecatelji)
            {
                natjecatelj.BrojBodova = brojBodova(natjecatelj.VirtualniTim);
            }

            return prvihDesetNajboljihNatjecatelja(natjecatelji);
        }

        [AcceptVerbs("GET")]
        [Route("Drzavno/{drzavaID:int}")]
        public List<Natjecatelji> Drzavno(int drzavaID)
        {

            var natjecatelji = _userManager.Users.Where(s => s.OvlastID == 4 && s.DrzavaID == drzavaID)
                                                 .Select(s => new Natjecatelji
                                                 {
                                                     UserName = s.UserName,
                                                     VirtualniTim = db.VirtualniTimovi.Where(e => e.ID == s.VirtualniTimID).FirstOrDefault()
                                                 }).ToList();

            foreach (Natjecatelji natjecatelj in natjecatelji)
            {
                natjecatelj.BrojBodova = brojBodova(natjecatelj.VirtualniTim);
            }

            return prvihDesetNajboljihNatjecatelja(natjecatelji);
        }

        [AcceptVerbs("GET")]
        [Route("Timsko/{timID:int}")]
        public List<Natjecatelji> Timsko(int timID)
        {

            var natjecatelji = _userManager.Users.Where(s => s.OvlastID == 4 && s.TimID == timID)
                                                 .Select(s => new Natjecatelji
                                                 {
                                                     UserName = s.UserName,
                                                     VirtualniTim = db.VirtualniTimovi.Where(e => e.ID == s.VirtualniTimID).FirstOrDefault()
                                                 }).ToList();

            foreach (Natjecatelji natjecatelj in natjecatelji)
            {
                natjecatelj.BrojBodova = brojBodova(natjecatelj.VirtualniTim);
            }

            return prvihDesetNajboljihNatjecatelja(natjecatelji);
        }

        [AcceptVerbs("GET")]
        [Route("Liga")]
        public List<Ekipe> Liga()
        {
            var ekipe = db.Timovi.Select(s => new Ekipe { Tim = s }).ToList();

            foreach (Ekipe ekipa in ekipe)
            {
                var utakmice = db.Utakmice.Where(s => s.LijeviTimID == ekipa.Tim.ID || s.DesniTimID == ekipa.Tim.ID).ToList();

                var brojBodova = 0;
                foreach (Utakmica utakmica in utakmice)
                {
                    if (utakmica.Pobjednik == 3)
                        brojBodova += 1;
                    if ((utakmica.LijeviTimID == ekipa.Tim.ID && utakmica.Pobjednik == 1)
                        || (utakmica.DesniTimID == ekipa.Tim.ID && utakmica.Pobjednik == 2))
                        brojBodova += 3;
                }

                ekipa.BrojBodova = brojBodova;
            }

            return prvihDesetNajboljihEkipa(ekipe);
        }

        [AcceptVerbs("GET")]
        [Route("Rezultati/{kolo?}")]
        public List<UtakmicaExport> Rezultati(int? kolo = null)
        {
            IQueryable<Utakmica> rezultati;

            if (kolo == null)
                rezultati = db.Utakmice.Where(s => s.Pobjednik != 0);
            else
                rezultati = db.Utakmice.Where(s => s.Pobjednik != 0 && s.Kolo == kolo);

            return rezultati.Select(s => new UtakmicaExport
            {
                ID = s.ID,
                Kolo = s.Kolo,
                Pobjednik = s.Pobjednik,
                LijeviBrojPogodaka = s.LijeviBrojPogodaka,
                DesniBrojPogodaka = s.DesniBrojPogodaka,
                LijeviTimID = s.LijeviTimID,
                DesniTimID = s.DesniTimID,
                LijeviTim = s.LijeviTim,
                DesniTim = s.DesniTim
            }).OrderByDescending(s => s.ID).Take(10).ToList();
        }

        [AcceptVerbs("GET")]
        [Route("NajkorisnijiIgraci")]
        public List<NajkorisnijiIgraci> NajkorisnijiIgraci()
        {
            List<IgracExport> igraci = db.Igraci.Select(s => new IgracExport
                                                    {
                                                        ID = s.ID,
                                                        Ime = s.Ime,
                                                        Prezime = s.Prezime,
                                                        BrojKapice = s.BrojKapice,
                                                        Tim = s.Tim
                                                    }).ToList();
            List<NajkorisnijiIgraci> result = new List<NajkorisnijiIgraci>();

            foreach (IgracExport igrac in igraci)
            {
                var tipovi = db.Dogadaji.Where(s => s.IgracID == igrac.ID)
                                        .Select(s => s.Tip)
                                        .ToList();

                int brojBodova = 0;
                foreach (Tip tip in tipovi)
                {
                    brojBodova += tip.Vrijednost;
                }

                result.Add(new NajkorisnijiIgraci
                {
                    ImePrezime = igrac.Ime + " " + igrac.Prezime,
                    BrojKapice = igrac.BrojKapice,
                    ImeTima = igrac.Tim.Naziv,
                    BrojBodova = brojBodova
                });
            }

            return prvihDesetNajkorisnijihIgraca(result);
        }

        private int brojBodova(VirtualniTim virtualniTim)
        {
            var igraci = db.Izabrani.Where(s => s.VirtualniTimID == virtualniTim.ID)
                                    .Select(s => new IgracExport
                                    {
                                        ID = s.Igrac.ID,
                                        Ime = s.Igrac.Ime,
                                        Prezime = s.Igrac.Prezime,
                                        BrojKapice = s.Igrac.BrojKapice,
                                        Vrijednost = s.Igrac.Vrijednost,
                                        TimID = s.Igrac.TimID,
                                        PozicijaID = s.Igrac.PozicijaID,
                                        Tim = s.Igrac.Tim,
                                        Pozicija = s.Igrac.Pozicija
                                    })
                                    .ToList();

            int timSum = 0;
            foreach (IgracExport igrac in igraci)
            {
                var vrijednosti = db.Dogadaji.Where(s => s.IgracID == igrac.ID)
                                          .Select(s => s.Tip.Vrijednost);
                
                foreach (int vrijednost in vrijednosti)
                {
                    timSum += vrijednost;
                }
            }
            return timSum;
        }

        private List<NajkorisnijiIgraci> prvihDesetNajkorisnijihIgraca(List<NajkorisnijiIgraci> najkorisnijiIgraci)
        {
            najkorisnijiIgraci = najkorisnijiIgraci.OrderByDescending(o => o.BrojBodova).ToList();

            List<NajkorisnijiIgraci> result = new List<NajkorisnijiIgraci>();
            for (int i = 0; i < 10 && i < najkorisnijiIgraci.Count; i++)
            {
                najkorisnijiIgraci[i].Mjesto = i + 1;
                result.Add(najkorisnijiIgraci[i]);
            }
            return result;
        }

        private List<Natjecatelji> prvihDesetNajboljihNatjecatelja(List<Natjecatelji> natjecatelji)
        {
            natjecatelji = natjecatelji.OrderByDescending(o => o.BrojBodova).ToList();

            List<Natjecatelji> result = new List<Natjecatelji>();
            for (int i = 0; i < 12 && i < natjecatelji.Count; i++)
            {
                natjecatelji[i].Mjesto = i + 1;
                result.Add(natjecatelji[i]);
            }
            return result;
        }

        private List<Ekipe> prvihDesetNajboljihEkipa(List<Ekipe> ekipe)
        {
            ekipe = ekipe.OrderByDescending(o => o.BrojBodova).ToList();

            List<Ekipe> result = new List<Ekipe>();
            for (int i = 0; i < 10 && i < ekipe.Count; i++)
            {
                ekipe[i].Mjesto = i + 1;
                result.Add(ekipe[i]);
            }
            return result;
        }
    }
}
