﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Models.ExportModels;

namespace VaterpoloLiga.Controllers
{
    [RoutePrefix("api/Utakmice")]
    public class UtakmiceController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/Utakmice
        [Route("")]
        public IQueryable<UtakmicaExport> GetUtakmice()
        {
            return db.Utakmice.Select(s => new UtakmicaExport
            {
                ID = s.ID,
                Kolo = s.Kolo,
                Pobjednik = s.Pobjednik,
                LijeviBrojPogodaka = s.LijeviBrojPogodaka,
                DesniBrojPogodaka = s.DesniBrojPogodaka,
                LijeviTimID = s.LijeviTimID,
                DesniTimID = s.DesniTimID,
                LijeviTim = s.LijeviTim,
                DesniTim = s.DesniTim
            }); ;
        }

        // GET: api/Utakmice/5
        [Route("{id:int}")]
        [ResponseType(typeof(Utakmica))]
        public IHttpActionResult GetUtakmica(int id)
        {
            Utakmica utakmica = db.Utakmice.Find(id);
            if (utakmica == null)
            {
                return NotFound();
            }

            return Ok(utakmica);
        }

        // GET: api/ZavrsiUtakmicu/5
        [AcceptVerbs("GET")]
        [Route("{id:int}/zavrsi")]
        [ResponseType(typeof(Boolean))]
        public IHttpActionResult ZavrsiUtakmicu(int id)
        {
            int utakmicaID = id;
            var dogadaji = db.Dogadaji.Where(s => s.UtakmicaID == utakmicaID).ToList();
            var utakmica = db.Utakmice.Where(s => s.ID == utakmicaID).Select(s => s).SingleOrDefault();

            if (utakmica.Pobjednik != 0)
                return Ok(false);

            foreach (var dogadaj in dogadaji)
            {
                if (dogadaj.TipID == 3 || dogadaj.TipID == 4 || dogadaj.TipID == 5)
                {
                    if (db.Igraci.Where(s => s.ID == dogadaj.IgracID).Select(s => s.TimID).SingleOrDefault() == utakmica.LijeviTimID)
                        utakmica.LijeviBrojPogodaka++;
                    else
                        utakmica.DesniBrojPogodaka++;
                }
            }

            if (utakmica.LijeviBrojPogodaka > utakmica.DesniBrojPogodaka)
                utakmica.Pobjednik = 1;
            else if (utakmica.LijeviBrojPogodaka < utakmica.DesniBrojPogodaka)
                utakmica.Pobjednik = 2;
            else
                utakmica.Pobjednik = 3;

            db.Entry(utakmica).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtakmicaExists(utakmica.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(true);
        }

        // PUT: api/Utakmice/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUtakmica(int id, Utakmica utakmica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != utakmica.ID)
            {
                return BadRequest();
            }

            db.Entry(utakmica).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtakmicaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Utakmice
        [ResponseType(typeof(Utakmica))]
        public IHttpActionResult PostUtakmica(Utakmica utakmica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Utakmice.Add(utakmica);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = utakmica.ID }, utakmica);
        }

        // DELETE: api/Utakmice/5
        [ResponseType(typeof(Utakmica))]
        public IHttpActionResult DeleteUtakmica(int id)
        {
            Utakmica utakmica = db.Utakmice.Find(id);
            if (utakmica == null)
            {
                return NotFound();
            }

            db.Utakmice.Remove(utakmica);
            db.SaveChanges();

            return Ok(utakmica);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UtakmicaExists(int id)
        {
            return db.Utakmice.Count(e => e.ID == id) > 0;
        }
    }
}