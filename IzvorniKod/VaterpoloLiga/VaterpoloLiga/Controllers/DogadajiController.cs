﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Models.ExportModels;

namespace VaterpoloLiga.Controllers
{
    [RoutePrefix("api/Dogadaji")]
    public class DogadajiController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        [AcceptVerbs("GET")]
        [Route("GetByUtakmica/{utakmicaID?}")]
        // GET: api/Dogadaji
        public IQueryable<DogadajExport> GetDogadaji(int? utakmicaID)
        {
            IQueryable<Dogadaj> dogadaji;

            if (utakmicaID == null)
                dogadaji = db.Dogadaji;
            else
                dogadaji = db.Dogadaji.Where(s => s.UtakmicaID == utakmicaID);

            return dogadaji.Select(s => new DogadajExport
            {
                ID = s.ID,
                Vrijeme = s.Vrijeme,
                TipID = s.TipID,
                IgracID = s.IgracID,
                UtakmicaID = s.UtakmicaID,
                Tip = s.Tip,
                Igrac = s.Igrac,
                Utakmica = s.Utakmica
            });
        }

        // GET: api/Dogadaji/5
        [ResponseType(typeof(Dogadaj))]
        public IHttpActionResult GetDogadaj(int id)
        {
            Dogadaj dogadaj = db.Dogadaji.Find(id);
            if (dogadaj == null)
            {
                return NotFound();
            }

            return Ok(dogadaj);
        }

        [AcceptVerbs("GET")]
        [Route("GetIgracaUtakmice/{utakmicaID?}")]
        public DogadajExport GetIgracaUtakmice(int? utakmicaID)
        {
            var dogadaj = db.Dogadaji.Where(s => s.UtakmicaID == utakmicaID && s.TipID == 2).FirstOrDefault();

            if (dogadaj == null)
                return null;

            return new DogadajExport
            {
                ID = dogadaj.ID,
                Vrijeme = dogadaj.Vrijeme,
                TipID = dogadaj.TipID,
                IgracID = dogadaj.IgracID,
                UtakmicaID = dogadaj.UtakmicaID,
                Tip = dogadaj.Tip,
                Igrac = dogadaj.Igrac,
                Utakmica = dogadaj.Utakmica
            };
        }

        // PUT: api/Dogadaji/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDogadaj(int id, Dogadaj dogadaj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dogadaj.ID)
            {
                return BadRequest();
            }

            db.Entry(dogadaj).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DogadajExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Dogadaji
        [ResponseType(typeof(Dogadaj))]
        public IHttpActionResult PostDogadaj(Dogadaj dogadaj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Dogadaji.Add(dogadaj);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = dogadaj.ID }, dogadaj);
        }

        // DELETE: api/Dogadaji/5
        [ResponseType(typeof(Dogadaj))]
        public IHttpActionResult DeleteDogadaj(int id)
        {
            Dogadaj dogadaj = db.Dogadaji.Find(id);
            if (dogadaj == null)
            {
                return NotFound();
            }

            db.Dogadaji.Remove(dogadaj);
            db.SaveChanges();

            return Ok(dogadaj);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DogadajExists(int id)
        {
            return db.Dogadaji.Count(e => e.ID == id) > 0;
        }
    }
}