﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Models.ExportModels;

namespace VaterpoloLiga.Controllers
{
    [RoutePrefix("api/Igraci")]
    public class IgraciController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/Igraci
        public IQueryable<IgracExport> GetIgraci(int? utakmicaID = null)
        {
            IQueryable<Igrac> igraci;

            if (utakmicaID == null)
                igraci = db.Igraci;
            else
            {
                Utakmica utakmica = db.Utakmice.Where(s => s.ID == utakmicaID).SingleOrDefault();

                igraci = db.Igraci.Where(s => s.TimID == utakmica.LijeviTimID || s.TimID == utakmica.DesniTimID);
            }

            return igraci.Select(s => new IgracExport
            {
                ID = s.ID,
                Ime = s.Ime,
                Prezime = s.Prezime,
                BrojKapice = s.BrojKapice,
                Vrijednost = s.Vrijednost,
                TimID = s.TimID,
                PozicijaID = s.PozicijaID,
                Tim = s.Tim,
                Pozicija = s.Pozicija
            });
        }

        [AcceptVerbs("GET")]
        [Route("GetIgraciFromTeam/{timID:int}")]
        public IQueryable<Igrac> GetIgraciFromTeam(int timID)
        {
            return db.Igraci.Where(s => s.TimID == timID);
        }

        // GET: api/Igraci/5
        [ResponseType(typeof(Igrac))]
        public IHttpActionResult GetIgrac(int id)
        {
            Igrac igrac = db.Igraci.Find(id);
            if (igrac == null)
            {
                return NotFound();
            }

            return Ok(new IgracExport
            {
                ID = igrac.ID,
                Ime = igrac.Ime,
                Prezime = igrac.Prezime,
                BrojKapice = igrac.BrojKapice,
                Vrijednost = igrac.Vrijednost,
                TimID = igrac.TimID,
                PozicijaID = igrac.PozicijaID,
                Tim = igrac.Tim,
                Pozicija = igrac.Pozicija
            });
        }

        // PUT: api/Igraci/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIgrac(List<Igrac> igraci)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach (Igrac igrac in igraci)
            {
                db.Entry(igrac).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Igraci
        [ResponseType(typeof(Igrac))]
        public IHttpActionResult PostIgrac(Igrac igrac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Igraci.Add(igrac);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = igrac.ID }, igrac);
        }

        // DELETE: api/Igraci/5
        [ResponseType(typeof(Igrac))]
        public IHttpActionResult DeleteIgrac(int id)
        {
            Igrac igrac = db.Igraci.Find(id);
            if (igrac == null)
            {
                return NotFound();
            }

            db.Igraci.Remove(igrac);
            db.SaveChanges();

            return Ok(igrac);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IgracExists(int id)
        {
            return db.Igraci.Count(e => e.ID == id) > 0;
        }
    }
}