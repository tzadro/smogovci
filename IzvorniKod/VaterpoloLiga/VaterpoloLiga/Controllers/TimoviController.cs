﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;

namespace VaterpoloLiga.Controllers
{
    public class TimoviController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/Timovi
        public IQueryable<Tim> GetTimovi()
        {
            return db.Timovi;
        }

        // GET: api/Timovi/5
        [ResponseType(typeof(Tim))]
        public IHttpActionResult GetTim(int id)
        {
            Tim tim = db.Timovi.Find(id);
            if (tim == null)
            {
                return NotFound();
            }

            return Ok(tim);
        }

        // PUT: api/Timovi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTim(int id, Tim tim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tim.ID)
            {
                return BadRequest();
            }

            db.Entry(tim).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TimExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Timovi
        [ResponseType(typeof(Tim))]
        public IHttpActionResult PostTim(Tim tim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Timovi.Add(tim);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tim.ID }, tim);
        }

        // DELETE: api/Timovi/5
        [ResponseType(typeof(Tim))]
        public IHttpActionResult DeleteTim(int id)
        {
            Tim tim = db.Timovi.Find(id);
            if (tim == null)
            {
                return NotFound();
            }

            db.Timovi.Remove(tim);
            db.SaveChanges();

            return Ok(tim);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TimExists(int id)
        {
            return db.Timovi.Count(e => e.ID == id) > 0;
        }
    }
}