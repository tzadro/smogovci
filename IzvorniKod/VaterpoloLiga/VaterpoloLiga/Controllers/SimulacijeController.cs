﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;
using VaterpoloLiga.Services;

namespace VaterpoloLiga.Controllers
{
    [RoutePrefix("api/Simulacije")]
    public class SimulacijeController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();
        private SimulationService _sim = new SimulationService();

        [AcceptVerbs("GET")]
        [Route("{n?}")]
        public bool Simuliraj(int n = 1)
        {
            var utakmice = db.Utakmice.Where(s => s.Pobjednik == 0).Take(n).OrderBy(s => s.ID).ToList();

            foreach (Utakmica utakmica in utakmice)
            {
                _sim.SimulirajUtakmicu(utakmica);
            }

            return true;
        }
    }
}
