﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;

namespace VaterpoloLiga.Controllers
{
    public class TipoviController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: api/Tipovi
        public IQueryable<Tip> GetTipovi()
        {
            return db.Tipovi.Where(s => s.ID != 1 && s.ID != 2);
        }

        // GET: api/Tipovi/5
        [ResponseType(typeof(Tip))]
        public IHttpActionResult GetTip(int id)
        {
            Tip tip = db.Tipovi.Find(id);
            if (tip == null)
            {
                return NotFound();
            }

            return Ok(tip);
        }

        // PUT: api/Tipovi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTip(List<Tip> tipovi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach (Tip tip in tipovi)
            {
                db.Entry(tip).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tipovi
        [ResponseType(typeof(Tip))]
        public IHttpActionResult PostTip(Tip tip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tipovi.Add(tip);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tip.ID }, tip);
        }

        // DELETE: api/Tipovi/5
        [ResponseType(typeof(Tip))]
        public IHttpActionResult DeleteTip(int id)
        {
            Tip tip = db.Tipovi.Find(id);
            if (tip == null)
            {
                return NotFound();
            }

            db.Tipovi.Remove(tip);
            db.SaveChanges();

            return Ok(tip);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipExists(int id)
        {
            return db.Tipovi.Count(e => e.ID == id) > 0;
        }
    }
}