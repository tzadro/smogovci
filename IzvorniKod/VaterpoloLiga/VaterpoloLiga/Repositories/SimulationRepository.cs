﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using VaterpoloLiga.DAL;
using VaterpoloLiga.Models;

namespace VaterpoloLiga.Repositories
{
    public class SimulationRepository
    {
        private DatabaseContext db = new DatabaseContext();

        // Predas ID tima i dobijes listu igraca u tom timu
        public List<Igrac> dohvatiIgraceIzTima(int timID)
        {
            return db.Igraci.Where(s => s.TimID == timID).ToList();
        }

        // Metoda za spremanje novog dogadaja u bazu
        public void spremiNoveDogadaje(List<Dogadaj> dogadaji)
        {
            foreach (Dogadaj dogadaj in dogadaji)
            {
                db.Dogadaji.Add(dogadaj);
            }
            db.SaveChanges();
        }


        // Metoda za spremanje postojece utakmice natrag u bazu
        public void spremiPostojecuUtakmicu(Utakmica utakmica)
        {
            db.Entry(utakmica).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public Tim getTimFromId(int timID)
        {
            return db.Timovi.Where(s => s.ID == timID).FirstOrDefault();
        }
    }
}