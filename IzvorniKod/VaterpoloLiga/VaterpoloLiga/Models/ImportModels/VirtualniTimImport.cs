﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.ImportModels
{
    public class VirtualniTimImport
    {
        public String Naziv { get; set; }

        public int Vratar { get; set; }
        public int Centar { get; set; }
        public int Bek { get; set; }
        public int Krilo1 { get; set; }
        public int Krilo2 { get; set; }
        public int Vanjski1 { get; set; }
        public int Vanjski2 { get; set; }
    }
}