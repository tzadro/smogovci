﻿
namespace VaterpoloLiga.Models
{
    public class Utakmica
    {
        public int ID { get; set; }
        public int Kolo { get; set; }
        public int Pobjednik { get; set; }
        public int LijeviBrojPogodaka { get; set; }
        public int DesniBrojPogodaka { get; set; }
        public int LijeviTimID { get; set; }
        public int DesniTimID { get; set; }
        
        public virtual Tim LijeviTim { get; set; }
        public virtual Tim DesniTim { get; set; }
    }
}