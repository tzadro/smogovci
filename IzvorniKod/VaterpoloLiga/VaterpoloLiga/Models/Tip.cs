﻿using System;

namespace VaterpoloLiga.Models
{
    public class Tip
    {
        public int ID { get; set; }
        public String Naziv { get; set; }
        public int Vrijednost { get; set; }
    }
}