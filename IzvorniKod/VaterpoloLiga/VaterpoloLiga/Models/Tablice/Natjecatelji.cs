﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.Tablice
{
    public class Natjecatelji
    {
        public int Mjesto { get; set; }
        public String UserName { get; set; }
        public VirtualniTim VirtualniTim { get; set; }
        public int BrojBodova { get; set; }
    }
}