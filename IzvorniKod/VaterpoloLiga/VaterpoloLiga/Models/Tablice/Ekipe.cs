﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.Tablice
{
    public class Ekipe
    {
        public int Mjesto { get; set; }
        public Tim Tim { get; set; }
        public int BrojBodova { get; set; }
    }
}