﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.Tablice
{
    public class NajkorisnijiIgraci
    {
        public int Mjesto { get; set; }
        public string ImePrezime { get; set; }
        public int BrojKapice { get; set; }
        public string ImeTima { get; set; }
        public int BrojBodova { get; set; }
    }
}