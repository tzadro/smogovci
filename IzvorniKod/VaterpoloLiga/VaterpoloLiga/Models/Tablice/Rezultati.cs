﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VaterpoloLiga.Models.ExportModels;

namespace VaterpoloLiga.Models.Tablice
{
    public class Rezultati
    {
        public UtakmicaExport Utakmica { get; set; }
        public int LijeviBrojPogodaka { get; set; }
        public int DesniBrojPogodaka { get; set; }
    }
}