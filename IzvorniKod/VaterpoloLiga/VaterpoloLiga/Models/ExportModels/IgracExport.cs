﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.ExportModels
{
    public class IgracExport
    {
        public int ID { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public int BrojKapice { get; set; }
        public int Vrijednost { get; set; }
        public int TimID { get; set; }
        public int PozicijaID { get; set; }
        public int BrojBodova { get; set; }

        public Tim Tim { get; set; }
        public Pozicija Pozicija { get; set; }
    }
}