﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.ExportModels
{
    public class DogadajExport
    {
        public int ID { get; set; }
        public int Vrijeme { get; set; }
        public int TipID { get; set; }
        public int IgracID { get; set; }
        public int UtakmicaID { get; set; }

        public Tip Tip { get; set; }
        public Igrac Igrac { get; set; }
        public Utakmica Utakmica { get; set; }
    }
}