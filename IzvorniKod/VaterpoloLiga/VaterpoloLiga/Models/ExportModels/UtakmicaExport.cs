﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.ExportModels
{
    public class UtakmicaExport
    {
        public int ID { get; set; }
        public int Kolo { get; set; }
        public int Pobjednik { get; set; }
        public int LijeviBrojPogodaka { get; set; }
        public int DesniBrojPogodaka { get; set; }
        public int LijeviTimID { get; set; }
        public int DesniTimID { get; set; }

        public Tim LijeviTim { get; set; }
        public  Tim DesniTim { get; set; }
    }
}