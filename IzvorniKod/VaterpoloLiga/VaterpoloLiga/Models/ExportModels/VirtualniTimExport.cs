﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models.ExportModels
{
    public class VirtualniTimExport
    {
        public int ID { get; set; }
        public String Naziv { get; set; }
        public int BrojBodova { get; set; }

        public ICollection<IgracExport> Igraci { get; set; }
    }
}