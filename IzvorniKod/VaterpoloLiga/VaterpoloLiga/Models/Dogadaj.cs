﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VaterpoloLiga.Models
{
    public class Dogadaj
    {
        public int ID { get; set; }
        public int Vrijeme { get; set; }
        public int TipID { get; set; }
        public int IgracID { get; set; }
        public int UtakmicaID { get; set; }

        public virtual Tip Tip { get; set; }
        public virtual Igrac Igrac { get; set; }
        public virtual Utakmica Utakmica { get; set; }
    }
}