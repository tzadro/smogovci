﻿using System;

namespace VaterpoloLiga.Models
{
    public class Pozicija
    {
        public int ID { get; set; }
        public String Naziv { get; set; }
    }
}