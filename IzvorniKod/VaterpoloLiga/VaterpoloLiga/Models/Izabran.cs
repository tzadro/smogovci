﻿
namespace VaterpoloLiga.Models
{
    public class Izabran
    {
        public int ID { get; set; }
        public int IgracID { get; set; }
        public int VirtualniTimID { get; set; }

        public Igrac Igrac { get; set; }
        public VirtualniTim VirtualniTim { get; set; }
    }
}