﻿using System;

namespace VaterpoloLiga.Models
{
    public class Drzava
    {
        public int ID { get; set; }
        public String Naziv { get; set; }
    }
}