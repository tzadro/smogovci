﻿using System;

namespace VaterpoloLiga.Models
{
    public class Tim
    {
        public int ID { get; set; }
        public String Naziv { get; set; }
    }
}