﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;

namespace VaterpoloLiga.Models
{
    public class Korisnik : IdentityUser
    {
        //public int ID { get; set; }

        /*
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
        */

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //public String Email { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public int OvlastID { get; set; }
        public int VirtualniTimID { get; set; }
        public int TimID { get; set; }
        public int DrzavaID { get; set; }
        
        /*
        public virtual Ovlast Ovlast { get; set; }
        public virtual VirtualniTim VirtualniTim { get; set; }
        public virtual Tim Tim { get; set; }
        public virtual Drzava Drzava { get; set; }
        */
    }
}