﻿using System;
using System.Collections.Generic;

namespace VaterpoloLiga.Models
{
    public class VirtualniTim
    {
        public int ID { get; set; }
        public String Naziv { get; set; }

        public virtual ICollection<Izabran> Izabrani { get; set; }
    }
}