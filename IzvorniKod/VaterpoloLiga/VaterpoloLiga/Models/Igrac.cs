﻿using System;

namespace VaterpoloLiga.Models
{
    public class Igrac
    {
        public int ID { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public int BrojKapice { get; set; }
        public int Vrijednost { get; set; }
        public int TimID { get; set; }
        public int PozicijaID { get; set; }

        public virtual Tim Tim { get; set; }
        public virtual Pozicija Pozicija { get; set; }
    }
}