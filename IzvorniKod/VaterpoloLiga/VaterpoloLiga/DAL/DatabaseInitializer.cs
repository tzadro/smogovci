﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaterpoloLiga.Models;
using VaterpoloLiga.Repositories;

namespace VaterpoloLiga.DAL
{
    // public class DatabaseInitializer : System.Data.Entity.DropCreateDatabaseAlways<DatabaseContext>
    public class DatabaseInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            //base.Seed(context);

            // drzave
            var drzave = new List<Drzava>
            {
                new Drzava{ Naziv="drzava01" },
                new Drzava{ Naziv="drzava02" },
                new Drzava{ Naziv="drzava03" },
                new Drzava{ Naziv="drzava04" },
                new Drzava{ Naziv="drzava05" }
            };
            drzave.ForEach(s => context.Drzave.Add(s));
            context.SaveChanges();

            // ovlasti
            var ovlasti = new List<Ovlast>
            {
                new Ovlast{ ID=1, Naziv="Administrator" },
                new Ovlast{ ID=2, Naziv="Službena osoba" },
                new Ovlast{ ID=3, Naziv="Član odbora" },
                new Ovlast{ ID=4, Naziv="Natjecatelj" }
            };
            ovlasti.ForEach(s => context.Ovlasti.Add(s));
            context.SaveChanges();

            // pozicije
            var pozicije = new List<Pozicija>
            {
                new Pozicija{ ID=1, Naziv="Vratar" },
                new Pozicija{ ID=2, Naziv="Bek" },
                new Pozicija{ ID=3, Naziv="Centar" },
                new Pozicija{ ID=4, Naziv="Krilo" },
                new Pozicija{ ID=5, Naziv="Vanjski" }
            };
            pozicije.ForEach(s => context.Pozicije.Add(s));
            context.SaveChanges();

            // TODO: dodati bergerovu tablicu u tablicu utakmica?

            // TODO: jesu li svi napisani?
            // tip dogadaja
            var tipovi = new List<Tip>
            {
                new Tip { ID=1, Naziv="Proračun", Vrijednost = 30 },
                new Tip { ID=2, Naziv="Najbolji igrač utakmice", Vrijednost = 10 },
                new Tip { ID=3, Naziv="Postignut zgoditak", Vrijednost = 10 },
                new Tip { ID=4, Naziv="Zgoditak s 5m", Vrijednost = 5 },
                new Tip { ID=5, Naziv="Zgoditak s igračem više", Vrijednost = 7 },
                new Tip { ID=6, Naziv="Dodavanje", Vrijednost = 15 },
                new Tip { ID=7, Naziv="Blokada", Vrijednost = 5 },
                new Tip { ID=8, Naziv="Ukradena lopta", Vrijednost = 3 },
                new Tip { ID=9, Naziv="Obrana vratara", Vrijednost = 5 },
                new Tip { ID=10, Naziv="Obrana šuta s 5m", Vrijednost = 8 },
                new Tip { ID=11, Naziv="Izgubljena lopta", Vrijednost = -8 },
                new Tip { ID=12, Naziv="Promašen šut", Vrijednost = -5 },
                new Tip { ID=13, Naziv="Promašen šut s 5m", Vrijednost = -3 },
                new Tip { ID=14, Naziv="Promašaj s igračem viška", Vrijednost = -7 },
                new Tip { ID=15, Naziv="Primljen zgoditak", Vrijednost = -3 },
                new Tip { ID=16, Naziv="Primljen zgoditak s 5m", Vrijednost = 0 },
                new Tip { ID=17, Naziv="Isključenje", Vrijednost = -5 },
                new Tip { ID=18, Naziv="Trajno isključenje", Vrijednost = -10 }
            };
            tipovi.ForEach(s => context.Tipovi.Add(s));
            context.SaveChanges();

            // TODO: provjeriti je li String.Format ispravan
            // sada pocinje excel tablica

            // timovi
            for (int i = 1; i < 13; i++)
                context.Timovi.Add(new Tim { ID = i, Naziv=String.Format("ekipa{0:00}", i) });
            context.SaveChanges();

            // igraci
            for (int i = 1; i < 13; i++)
            {
                context.Igraci.Add(new Igrac { BrojKapice = 1, Ime = String.Format("e{0:00}igrač01ime", i), 
                    Prezime = String.Format("e{0:00}igrač01prezime", i), Vrijednost = 1, TimID = i, PozicijaID = 1 });
                context.Igraci.Add(new Igrac { BrojKapice = 2, Ime = String.Format("e{0:00}igrač02ime", i), 
                    Prezime = String.Format("e{0:00}igrač02prezime", i), Vrijednost = 2, TimID = i, PozicijaID = 2 });
                context.Igraci.Add(new Igrac { BrojKapice = 3, Ime = String.Format("e{0:00}igrač03ime", i), 
                    Prezime = String.Format("e{0:00}igrač03prezime", i), Vrijednost = 3, TimID = i, PozicijaID = 2 });
                context.Igraci.Add(new Igrac { BrojKapice = 4, Ime = String.Format("e{0:00}igrač04ime", i), 
                    Prezime = String.Format("e{0:00}igrač04prezime", i), Vrijednost = 4, TimID = i, PozicijaID = 3 });
                context.Igraci.Add(new Igrac { BrojKapice = 5, Ime = String.Format("e{0:00}igrač05ime", i), 
                    Prezime = String.Format("e{0:00}igrač05prezime", i), Vrijednost = 3, TimID = i, PozicijaID = 3 });
                context.Igraci.Add(new Igrac { BrojKapice = 6, Ime = String.Format("e{0:00}igrač06ime", i), 
                    Prezime = String.Format("e{0:00}igrač06prezime", i), Vrijednost = 2, TimID = i, PozicijaID = 4 });
                context.Igraci.Add(new Igrac { BrojKapice = 7, Ime = String.Format("e{0:00}igrač07ime", i), 
                    Prezime = String.Format("e{0:00}igrač07prezime", i), Vrijednost = 1, TimID = i, PozicijaID = 4 });
                context.Igraci.Add(new Igrac { BrojKapice = 8, Ime = String.Format("e{0:00}igrač08ime", i), 
                    Prezime = String.Format("e{0:00}igrač08prezime", i), Vrijednost = 2, TimID = i, PozicijaID = 4 });
                context.Igraci.Add(new Igrac { BrojKapice = 9, Ime = String.Format("e{0:00}igrač09ime", i), 
                    Prezime = String.Format("e{0:00}igrač09prezime", i), Vrijednost = 3, TimID = i, PozicijaID = 4 });
                context.Igraci.Add(new Igrac { BrojKapice = 10, Ime = String.Format("e{0:00}igrač10ime", i), 
                    Prezime = String.Format("e{0:00}igrač10prezime", i), Vrijednost = 4, TimID = i, PozicijaID = 5 });
                context.Igraci.Add(new Igrac { BrojKapice = 11, Ime = String.Format("e{0:00}igrač11ime", i), 
                    Prezime = String.Format("e{0:00}igrač11prezime", i), Vrijednost = 3, TimID = i, PozicijaID = 5 });
                context.Igraci.Add(new Igrac { BrojKapice = 12, Ime = String.Format("e{0:00}igrač12ime", i), 
                    Prezime = String.Format("e{0:00}igrač12prezime", i), Vrijednost = 2, TimID = i, PozicijaID = 5 });
                context.Igraci.Add(new Igrac { BrojKapice = 13, Ime = String.Format("e{0:00}igrač13ime", i), 
                    Prezime = String.Format("e{0:00}igrač13prezime", i), Vrijednost = 1, TimID = i, PozicijaID = 1 });
            };
            context.SaveChanges();

            // utakmice
            for (int i = 1; i < 7; i++)
                context.Utakmice.Add(new Utakmica { Kolo = 1, LijeviTimID = i, DesniTimID = 13 - i });

            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 12, DesniTimID = 7 });
            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 8, DesniTimID = 6 });
            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 9, DesniTimID = 5 });
            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 10, DesniTimID = 4 });
            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 11, DesniTimID = 3 });
            context.Utakmice.Add(new Utakmica { Kolo = 2, LijeviTimID = 1, DesniTimID = 2 });

            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 2, DesniTimID = 12 });
            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 3, DesniTimID = 1 });
            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 4, DesniTimID = 11 });
            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 5, DesniTimID = 10 });
            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 6, DesniTimID = 9 });
            context.Utakmice.Add(new Utakmica { Kolo = 3, LijeviTimID = 7, DesniTimID = 8 });

            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 12, DesniTimID = 8 });
            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 9, DesniTimID = 7 });
            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 10, DesniTimID = 6 });
            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 11, DesniTimID = 5 });
            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 1, DesniTimID = 4 });
            context.Utakmice.Add(new Utakmica { Kolo = 4, LijeviTimID = 2, DesniTimID = 3 });

            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 3, DesniTimID = 12 });
            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 4, DesniTimID = 2 });
            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 5, DesniTimID = 1 });
            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 6, DesniTimID = 11 });
            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 7, DesniTimID = 10 });
            context.Utakmice.Add(new Utakmica { Kolo = 5, LijeviTimID = 8, DesniTimID = 9 });

            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 12, DesniTimID = 9 });
            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 10, DesniTimID = 8 });
            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 11, DesniTimID = 7 });
            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 1, DesniTimID = 6 });
            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 2, DesniTimID = 5 });
            context.Utakmice.Add(new Utakmica { Kolo = 6, LijeviTimID = 3, DesniTimID = 4 });

            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 4, DesniTimID = 12 });
            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 5, DesniTimID = 3 });
            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 6, DesniTimID = 2 });
            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 7, DesniTimID = 1 });
            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 8, DesniTimID = 11 });
            context.Utakmice.Add(new Utakmica { Kolo = 7, LijeviTimID = 9, DesniTimID = 10 });

            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 12, DesniTimID = 10 });
            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 11, DesniTimID = 9 });
            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 1, DesniTimID = 8 });
            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 2, DesniTimID = 7 });
            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 3, DesniTimID = 6 });
            context.Utakmice.Add(new Utakmica { Kolo = 8, LijeviTimID = 4, DesniTimID = 5 });

            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 5, DesniTimID = 12 });
            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 6, DesniTimID = 4 });
            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 7, DesniTimID = 3 });
            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 8, DesniTimID = 2 });
            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 9, DesniTimID = 1 });
            context.Utakmice.Add(new Utakmica { Kolo = 9, LijeviTimID = 10, DesniTimID = 11 });

            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 12, DesniTimID = 11 });
            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 1, DesniTimID = 10 });
            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 2, DesniTimID = 9 });
            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 3, DesniTimID = 8 });
            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 4, DesniTimID = 7 });
            context.Utakmice.Add(new Utakmica { Kolo = 10, LijeviTimID = 5, DesniTimID = 6 });

            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 6, DesniTimID = 12 });
            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 7, DesniTimID = 5 });
            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 8, DesniTimID = 4 });
            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 9, DesniTimID = 3 });
            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 10, DesniTimID = 2 });
            context.Utakmice.Add(new Utakmica { Kolo = 11, LijeviTimID = 11, DesniTimID = 1 });

            context.SaveChanges();

            // korisnici
            addUsers(context);
        }

        private void addUsers(DatabaseContext context)
        {
            UserManager<Korisnik> _userManager = new UserManager<Korisnik>(new UserStore<Korisnik>(context));

            // natjecatelj
            var virtualniTim = new VirtualniTim { Naziv = "Test Tim" };
            context.VirtualniTimovi.Add(virtualniTim);
            context.SaveChanges();

            var izabrani = new List<Izabran>
            {
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 1 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 2 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 4 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 6 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 7 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 10 },
                new Izabran { VirtualniTimID = virtualniTim.ID, IgracID = 11 }
            };
            izabrani.ForEach(s => context.Izabrani.Add(s));
            context.SaveChanges();

            var natjecatelj = new Korisnik
            {
                UserName = "natjecatelj",
                Password = "natjecatelj",
                ConfirmPassword = "natjecatelj",
                Ime = "nat_ime",
                Prezime = "nat_prezime",
                OvlastID = 4,
                VirtualniTimID = virtualniTim.ID,
                TimID = 1,
                DrzavaID = 1
            };
            _userManager.Create(natjecatelj, natjecatelj.Password);

            // sluzbena osoba
            var sluzbenaosoba = new Korisnik
            {
                UserName = "sluzbenaosoba",
                Password = "sluzbenaosoba",
                ConfirmPassword = "sluzbenaosoba",
                Ime = "sluos_ime",
                Prezime = "sluos_prezime",
                OvlastID = 2
            };
            _userManager.Create(sluzbenaosoba, sluzbenaosoba.Password);

            // admin
            var administrator = new Korisnik
            {
                UserName = "administrator",
                Password = "administrator",
                ConfirmPassword = "administrator",
                Ime = "admin_ime",
                Prezime = "admin_prezime",
                OvlastID = 1
            };
            _userManager.Create(administrator, administrator.Password);

            // clan odbora
            var clanodbora = new Korisnik
            {
                UserName = "clanodbora",
                Password = "clanodbora",
                ConfirmPassword = "clanodbora",
                Ime = "clodb_ime",
                Prezime = "clodb_prezime",
                OvlastID = 3
            };
            _userManager.Create(clanodbora, clanodbora.Password);
        }
    }
}