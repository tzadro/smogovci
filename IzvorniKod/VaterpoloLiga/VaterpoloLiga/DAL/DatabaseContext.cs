﻿using VaterpoloLiga.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VaterpoloLiga.DAL
{
    public class DatabaseContext : IdentityDbContext<IdentityUser>
    {
        public DatabaseContext() : base("DatabaseContext", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Dogadaj> Dogadaji { get; set; }
        public DbSet<Drzava> Drzave { get; set; }
        public DbSet<Igrac> Igraci { get; set; }
        public DbSet<Izabran> Izabrani { get; set; }
        public DbSet<Ovlast> Ovlasti { get; set; }
        public DbSet<Pozicija> Pozicije { get; set; }
        public DbSet<Tim> Timovi { get; set; }
        public DbSet<Tip> Tipovi { get; set; }
        public DbSet<Utakmica> Utakmice { get; set; }
        public DbSet<VirtualniTim> VirtualniTimovi { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}