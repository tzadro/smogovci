﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VaterpoloLiga.Models;
using VaterpoloLiga.Repositories;

namespace VaterpoloLiga.Services
{
    public class SimulationService
    {
        private SimulationRepository _repo = new SimulationRepository();
        private int brojPogodakaPoUtakmici = 18;
        private int brojPucanja = 60;
        private Dictionary<int, int> brojIskljucenja = new Dictionary<int, int>();
        private Dictionary<int, int> trenutnoIskljuceni = new Dictionary<int, int>();
        private List<Igrac> trajnoIskljuceni = new List<Igrac>();
        private int posjed;
        private List<Dogadaj> dogadaji = new List<Dogadaj>();
        private Random rand = new Random();
        int domaci_golovi, gosti_golovi;
       

        // Metoda prima utakmicu kao argument i generira dogadaje za tu utakmicu,
        // na kraju azurira atribute Pobjednik, LijeviBrojPogodaka i DesniBrojPogodaka
        // te sprema utakmicu natrag u bazu

        public Igrac linq_golman(List<Igrac> igraci)
        {
            Igrac return_igrac = new Igrac();
            var rez = from player in igraci
                      where player.PozicijaID == 1
                      select player;

            foreach (Igrac golman in rez)
            {
                return_igrac = golman;
                break;
            }
            return return_igrac;
                
        }

        public Igrac linq_igrac(List<Igrac> igraci_u_ekipi)
        {
            var selected_players = from players in igraci_u_ekipi
                                   where players.PozicijaID != 1  && trenutnoIskljuceni.ContainsKey(players.ID) == false && trajnoIskljuceni.Contains(players) == false
                                   select players;
           // Random rand = new Random();
            List<Igrac> lista_igraca = selected_players.ToList();
            return lista_igraca[rand.Next(0, lista_igraca.Count)];
        }

        public void change_time(List <int> list_of_keys,int new_time)
        {
            foreach (int key in list_of_keys)
            {
                trenutnoIskljuceni[key] -= new_time;
                if (trenutnoIskljuceni[key] <= 0)
                    trenutnoIskljuceni.Remove(key);
            }
        }

        public void SimulirajUtakmicu(Utakmica utakmica)
        {
            dogadaji.Clear();
            brojIskljucenja.Clear();
            trenutnoIskljuceni.Clear();
            trajnoIskljuceni.Clear();
            Tim lijeviTim = _repo.getTimFromId(utakmica.LijeviTimID);
            Tim desniTim = _repo.getTimFromId(utakmica.DesniTimID);

            List<Igrac> igraciDesnogTima = _repo.dohvatiIgraceIzTima(desniTim.ID);
            List<Igrac> igraciLijevogTima = _repo.dohvatiIgraceIzTima(lijeviTim.ID);
            domaci_golovi = 0;
            gosti_golovi = 0;
            int ukupno_vrijeme = 0;

            posjed = lijeviTim.ID;
            for (; ; )
            {
                
                int new_time = rand.Next(35, 45);
                ukupno_vrijeme += new_time;

                 if (ukupno_vrijeme > 32 * 60)
                     break;
               List<int> list_of_keys = new List<int>();
                list_of_keys = trenutnoIskljuceni.Keys.ToList();
                change_time(list_of_keys, new_time);
           //      trenutnoIskljuceni.Clear();
                
                    //mjesto za linq, iskljuceni igraci se ne mogu izabrati

                Igrac igrac1 = (posjed == lijeviTim.ID) ? linq_igrac(igraciLijevogTima) : linq_igrac(igraciDesnogTima);
                Igrac igrac2 =(posjed == desniTim.ID) ? linq_igrac(igraciLijevogTima) : linq_igrac(igraciDesnogTima);
                Igrac igrac3 = (posjed == lijeviTim.ID) ? linq_igrac(igraciLijevogTima) : linq_igrac(igraciDesnogTima);
                Igrac golman = (posjed == desniTim.ID) ? linq_golman(igraciLijevogTima):linq_golman(igraciDesnogTima);
                
                    Boolean mijenjati = dogadaj(ukupno_vrijeme, utakmica, posjed, igrac1, igrac2, igrac3, golman, utakmica.ID);
                    if (mijenjati)
                    {
                        posjed = (posjed == desniTim.ID ? lijeviTim.ID : desniTim.ID);
                    }

            }
            _repo.spremiNoveDogadaje(dogadaji);
            utakmica.LijeviBrojPogodaka = domaci_golovi;
            utakmica.DesniBrojPogodaka = gosti_golovi;
            if (utakmica.LijeviBrojPogodaka == utakmica.DesniBrojPogodaka)
                utakmica.Pobjednik = 3;
            else if (utakmica.LijeviBrojPogodaka > utakmica.DesniBrojPogodaka)
                utakmica.Pobjednik = 1;
            else
                utakmica.Pobjednik = 2;
            _repo.spremiPostojecuUtakmicu(utakmica);
        }

        private Boolean dogadaj(int vrijeme, Utakmica utakmica, int posjed, Igrac igrac1, Igrac igrac2, Igrac igrac3, Igrac golman, int utakmicaID)
        {
 
            int randNum = rand.Next(0,101);

            //pogodak
            if (randNum < 50)
            {
                bool igrac_vise = (trenutnoIskljuceni.Count < 1) ? false : true;
                pucanje(vrijeme, igrac1, igrac2, igrac3, golman, igrac_vise, utakmicaID,utakmica.LijeviTimID);

                return true;
            }
            //iskljucenje
            else if (randNum < 92)
            {
                iskljucenje(vrijeme, igrac1.TimID != posjed ? igrac1 : igrac2, utakmicaID);
                return false;
            }

            ukradenaIzgubljena(vrijeme, igrac1, igrac2, utakmicaID);

            return true;
            
        }

        private void ukradenaIzgubljena(int vrijeme, Igrac igrac1, Igrac igrac2, int utakmicaID)
        {
            Dogadaj noviAktivni = new Dogadaj();
            noviAktivni.Vrijeme = vrijeme/60;
            noviAktivni.IgracID = igrac2.ID;

            Dogadaj noviPasivni = new Dogadaj();
            noviPasivni.Vrijeme = vrijeme/60;
            noviPasivni.IgracID = igrac1.ID;

            noviAktivni.UtakmicaID = noviPasivni.UtakmicaID = utakmicaID;

            noviPasivni.TipID = 11; //pasivni -> izgubljena lopta
            noviAktivni.TipID = 8; //aktivni -> krade loptu

            dogadaji.Add(noviAktivni);
            dogadaji.Add(noviPasivni);
        }

        private void iskljucenje(int vrijeme, Igrac igrac, int utakmicaID)
        {
            Dogadaj novi = new Dogadaj();

            novi.Vrijeme = vrijeme /60;
            novi.UtakmicaID = utakmicaID;
            novi.IgracID = igrac.ID;

            if (brojIskljucenja.ContainsKey(igrac.ID))
            {
                brojIskljucenja[igrac.ID]++; 
                if (brojIskljucenja[igrac.ID] >= 3)
                {
                    trajnoIskljuceni.Add(igrac);
                    novi.TipID = 18;
                }
                else if(brojIskljucenja[igrac.ID] < 3)
                {
                    novi.TipID = 17;
                    if (trenutnoIskljuceni.ContainsKey(igrac.ID) == false)
                        trenutnoIskljuceni.Add(igrac.ID, 20);
                } 
            }
            else
            {
                brojIskljucenja.Add(igrac.ID, 1);
                novi.TipID = 17;
                trenutnoIskljuceni.Add(igrac.ID, 20);
            }
            dogadaji.Add(novi);
            
        }

        private void pucanje(int vrijeme, Igrac igrac1, Igrac igrac2, Igrac igrac3, Igrac golman, bool igracVise, int utakmicaID,int lijeviTimID)
        {
            //aktivni dogadaj je nad igracem koji izvodi pucanje
            Dogadaj noviAktivni = new Dogadaj();
            //pasivni dogadaj moze biti nad igracem koji blokira udarac ili golmanom (koji brani ili prima gol)
            Dogadaj noviPasivni = new Dogadaj();
            //dodatni dogadaj je nad igracem koji (mozda) asistira
            Dogadaj dodatni = new Dogadaj();
            
            int randNum = rand.Next(0, 34);
            noviAktivni.IgracID = igrac1.ID;
            noviAktivni.Vrijeme = vrijeme/60;
            noviAktivni.UtakmicaID = utakmicaID;

            noviPasivni.Vrijeme = vrijeme/60;
            noviPasivni.UtakmicaID = utakmicaID;

            dodatni.Vrijeme = vrijeme / 60 ;
            dodatni.IgracID = igrac3.ID;
            dodatni.TipID = 6;
            dodatni.UtakmicaID = utakmicaID;

            //Dogodio se zgoditak
            if (randNum < 24)
            {
                //pasivni igrac je golman
                noviPasivni.IgracID = golman.ID;
                //zgoditak s 5 m
                if (randNum <= 2)
                {                    
                    noviAktivni.TipID = 4;                    
                    noviPasivni.TipID = 16;
                }
                //zgoditak s igracem vise
                else if (igracVise)
                {
                    noviAktivni.TipID = 5;
                    noviPasivni.TipID = 15;
                    dogadaji.Add(dodatni);
                }
                //obican zgoditak
                else
                {
                    noviAktivni.TipID = 3;
                    noviPasivni.TipID = 15;
                    dogadaji.Add(dodatni);
                }
                dogadaji.Add(noviAktivni);
                dogadaji.Add(noviPasivni);
                if (igrac1.TimID == lijeviTimID)
                    domaci_golovi++;
                else
                    gosti_golovi++;
                trenutnoIskljuceni.Clear();
            }
            //Blokada
            else if (randNum <= 21)
            {              
                noviPasivni.IgracID = igrac2.ID;
                noviPasivni.TipID = 7;
                //promasen sut s igracem vise - blokada s igračem više
                if (igracVise)
                {
                    noviAktivni.TipID = 14;
                }
                
                //blokada-bez igrača više

                else
                {
                    noviAktivni.TipID = 12;
                }

                dogadaji.Add(noviAktivni);
                dogadaji.Add(noviPasivni);


            }

            //Obrana golmana
            else
            {
                noviPasivni.IgracID = golman.ID;

                if (randNum <= 41)
                {
                    //obrana golmana sa 5 m
                    noviPasivni.TipID = 10;
                    noviAktivni.TipID = 13;
                }
                else
                {
                    //obrana golmana
                    noviAktivni.TipID = 12;
                    noviPasivni.TipID = 9;
                }
                dogadaji.Add(noviPasivni);
                dogadaji.Add(noviAktivni);
               
            }
        }

    }
}