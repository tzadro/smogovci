﻿vaterpoloLiga.factory('tabliceService', ['$http', function ($http) {
    var tabliceService = {};

    tabliceService.getGlobalno = function () {
        return $http.get('api/Tablice/Globalno');
    }

    tabliceService.getDrzavno = function (drzavaID) {
        return $http.get('api/Tablice/Drzavno/' + drzavaID);
    }

    tabliceService.getTimsko = function (timID) {
        return $http.get('api/Tablice/Timsko/' + timID);
    }

    tabliceService.getLiga = function () {
        return $http.get('api/Tablice/Liga');
    }

    tabliceService.getRezultati = function (koloID) {
        if (koloID == null)
            return $http.get('api/Tablice/Rezultati');
        else
            return $http.get('api/Tablice/Rezultati/' + koloID);
    }

    tabliceService.najkorisnijiIgraci = function () {
        return $http.get('api/Tablice/NajkorisnijiIgraci');
    }

    return tabliceService;
}]);