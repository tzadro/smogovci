﻿vaterpoloLiga.factory('virtualniTimoviService', ['$http', function ($http) {
    var virtualniTimoviService = {};

    virtualniTimoviService.getVirtualniTimById = function (virtualniTimID) {
        return $http.get('api/VirtualniTimovi/' + virtualniTimID);
    }

    virtualniTimoviService.createVirtualniTim = function (virtualniTim) {
        return $http.post('api/VirtualniTimovi', virtualniTim);
    }

    return virtualniTimoviService;
}]);