﻿vaterpoloLiga.factory('igraciService', ['$http', function ($http) {
    var igraciService = {};

    igraciService.getIgraci = function () {
        return $http.get('api/Igraci');
    }

    igraciService.getIgraciFromTeam = function (timID) {
        return $http.get('api/Igraci/GetIgraciFromTeam/' + timID);
    }

    igraciService.getIgraciByUtakmica = function (utakmicaID) {
        return $http.get('api/Igraci?utakmicaID=' + utakmicaID);
    }

    igraciService.saveIgraci = function (igraci) {
        return $http.put('api/Igraci', igraci);
    }

    return igraciService;
}]);