﻿vaterpoloLiga.factory('tipoviService', ['$http', function ($http) {
    var tipoviService = {};

    tipoviService.getTipovi = function () {
        return $http.get('api/Tipovi');
    }

    tipoviService.saveTipovi = function (tipovi) {
        return $http.put('api/Tipovi', tipovi);
    }

    return tipoviService;
}]);