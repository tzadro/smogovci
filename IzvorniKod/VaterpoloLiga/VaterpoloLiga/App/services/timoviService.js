﻿vaterpoloLiga.factory('timoviService', ['$http', function ($http) {
    var timoviService = {};

    timoviService.getTimById = function (timID) {
        return $http.get('api/Timovi/' + timID);
    }

    timoviService.getTimovi = function () {
        return $http.get('api/Timovi');
    }

    return timoviService;
}]);