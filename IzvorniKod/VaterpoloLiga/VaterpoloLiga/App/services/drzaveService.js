﻿vaterpoloLiga.factory('drzaveService', ['$http', function ($http) {
    var drzaveService = {};

    drzaveService.getDrzavaById = function (drzavaID) {
        return $http.get('api/Drzave/' + drzavaID);
    }

    drzaveService.getDrzave = function () {
        return $http.get('api/Drzave');
    }

    return drzaveService;
}]);