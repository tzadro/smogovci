﻿'use strict';
vaterpoloLiga.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = 'http://localhost:2788/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        id: "",
        ime: "",
        prezime: "",
        ovlastID: "",
        virtualniTimID: "",
        timID: "",
        drzavaID: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.id = response.id;
            _authentication.ime = response.ime;
            _authentication.prezime = response.prezime;
            _authentication.ovlastID = response.ovlastID;
            _authentication.virtualniTimID = response.virtualniTimID;
            _authentication.timID = response.timID;
            _authentication.drzavaID = response.drzavaID;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.id = "";
        _authentication.ime = "";
        _authentication.prezime = "";
        _authentication.ovlastID = "";
        _authentication.virtualniTimID = "";
        _authentication.timID = "";
        _authentication.drzavaID = "";
    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);