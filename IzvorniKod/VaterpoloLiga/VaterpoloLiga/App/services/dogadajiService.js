﻿vaterpoloLiga.factory('dogadajiService', ['$http', function ($http) {
    var dogadajiService = {};

    dogadajiService.getDogadajiByUtakmica = function (utakmicaID) {
        return $http.get('api/Dogadaji/GetByUtakmica/' + utakmicaID);
    }

    dogadajiService.getIgracUtakmice = function (utakmicaID) {
        return $http.get('api/Dogadaji/GetIgracaUtakmice/' + utakmicaID);
    }

    dogadajiService.dodajNoviDogadaj = function (noviDogadaj) {
        return $http.post('api/Dogadaji/', noviDogadaj);
    }

    return dogadajiService;
}]);