﻿vaterpoloLiga.factory('utakmiceService', ['$http', function ($http) {
    var utakmiceService = {};

    utakmiceService.getUtakmice = function () {
        return $http.get('api/Utakmice');
    }

    utakmiceService.zavrsiUtakmicu = function (utakmicaID) {
        return $http.get('api/Utakmice/' + utakmicaID + '/zavrsi');
    }

    return utakmiceService;
}]);