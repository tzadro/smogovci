﻿vaterpoloLiga.factory('simulationService', ['$http', function ($http) {
    var simulationService = {};

    simulationService.simuliraj = function (n) {
        if (n == null)
            return $http.get('api/Simulacije');
        else
            return $http.get('api/Simulacije/' + n);
    }

    return simulationService;
}]);