﻿vaterpoloLiga.controller('igracUtakmiceController', ['$scope', 'utakmiceService', 'igraciService', 'dogadajiService', function ($scope, utakmiceService, igraciService, dogadajiService) {

    $scope.aktivnoKolo = null;
    $scope.utakmice = null;
    $scope.igraci = null;
    $scope.successMessage = null;

    $scope.kola = [
        { id: 1, finished: 0 },
        { id: 2, finished: 0 },
        { id: 3, finished: 0 },
        { id: 4, finished: 0 },
        { id: 5, finished: 0 },
        { id: 6, finished: 0 },
        { id: 7, finished: 0 },
        { id: 8, finished: 0 },
        { id: 9, finished: 0 },
        { id: 10, finished: 0 },
        { id: 11, finished: 0 },
    ];

    function resetData() {

        $scope.trenutnaUtakmica = null;
        $scope.successMessage = null;
        $scope.igracUtakmice = null;

        $scope.noviDogadaj = {
            utakmicaID: "",
            vrijeme: "33",
            tipID: "2",
            igracID: ""
        }
    };
    resetData();

    function getData() {
        utakmiceService.getUtakmice().then(function (response) {
            $scope.utakmice = response.data;
        });
    }
    getData();

    $scope.showUtakmica = function (utakmica) {

        resetData();

        $scope.trenutnaUtakmica = utakmica;
        $scope.noviDogadaj.utakmicaID = utakmica.id;

        igraciService.getIgraciByUtakmica(utakmica.id).then(function (response) {
            $scope.igraci = response.data;
        });

        dogadajiService.getIgracUtakmice(utakmica.id).then(function (response) {
            console.log(response);
            if (response.data != null) {
                $scope.igracUtakmice = response.data;
                $scope.noviDogadaj.igracID = $scope.igracUtakmice.id;
            }
        })
    }

    $scope.promijeniKolo = function (kolo) {
        $scope.aktivnoKolo = kolo;
        $scope.utakmiceKola = [];
        for (var id in $scope.utakmice)
            if ($scope.utakmice[id].kolo == kolo)
                $scope.utakmiceKola.push($scope.utakmice[id]);
    }

    $scope.spremiIgracaUtakmice = function () {
        dogadajiService.dodajNoviDogadaj($scope.noviDogadaj).then(function (response) {
            $scope.successMessage = "Uspješno spremljeno!"
        });
    }
}]);