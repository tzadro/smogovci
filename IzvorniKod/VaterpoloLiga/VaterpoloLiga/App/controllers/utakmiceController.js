﻿vaterpoloLiga.controller('utakmiceController', ['$scope', 'utakmiceService', 'tipoviService', 'igraciService', 'dogadajiService', function ($scope, utakmiceService, tipoviService, igraciService, dogadajiService) {

    $scope.aktivnoKolo = null;
    $scope.utakmice = null;
    $scope.utakmiceKola = null;
    $scope.tipovi = null;
    $scope.igraci = null;
    $scope.dogadaji = null;

    $scope.kola = [
        { id: 1, finished: 0 },
        { id: 2, finished: 0 },
        { id: 3, finished: 0 },
        { id: 4, finished: 0 },
        { id: 5, finished: 0 },
        { id: 6, finished: 0 },
        { id: 7, finished: 0 },
        { id: 8, finished: 0 },
        { id: 9, finished: 0 },
        { id: 10, finished: 0 },
        { id: 11, finished: 0 },
    ];

    function resetData() {

        $scope.dogadaji = null;
        $scope.trenutnaUtakmica = null;

        $scope.noviDogadaj = {
            utakmicaID: "",
            vrijeme: "",
            tipID: "",
            igracID: ""
        }

        $scope.brojPogodaka = {
            lijeviTim: 0,
            desniTim: 0
        }
    };
    resetData();

    function getData() {
        utakmiceService.getUtakmice().then(function (response) {
            $scope.utakmice = response.data;

            for (id in $scope.kola)
                $scope.kola[id].finished = 0;
            for (id in $scope.utakmice)
                if ($scope.utakmice[id].pobjednik != 0)
                    $scope.kola[$scope.utakmice[id].kolo - 1].finished++;
        });

        tipoviService.getTipovi().then(function (response) {
            $scope.tipovi = response.data;
        });
    }
    getData();

    $scope.showUtakmica = function (utakmica)  {

        resetData();

        $scope.trenutnaUtakmica = utakmica;
        $scope.noviDogadaj.utakmicaID = utakmica.id;

        dogadajiService.getDogadajiByUtakmica(utakmica.id).then(function (response) {
            $scope.dogadaji = response.data;
            for (var dogadaj in $scope.dogadaji) {
                if ($scope.dogadaji[dogadaj].tipID == 3 || $scope.dogadaji[dogadaj].tipID == 4 || $scope.dogadaji[dogadaj].tipID == 5) {
                    if ($scope.dogadaji[dogadaj].igrac.timID == $scope.trenutnaUtakmica.lijeviTimID)
                        $scope.brojPogodaka.lijeviTim++;
                    else
                        $scope.brojPogodaka.desniTim++;
                }
            }
        });

        igraciService.getIgraciByUtakmica(utakmica.id).then(function (response) {
            $scope.igraci = response.data;
        });
    }

    $scope.dodajNoviDogadaj = function () {
        dogadajiService.dodajNoviDogadaj($scope.noviDogadaj).then(function (response) {
            $scope.showUtakmica($scope.trenutnaUtakmica);
        });
    }

    $scope.zavrsiUtakmicu = function () {
        utakmiceService.zavrsiUtakmicu($scope.trenutnaUtakmica.id).then(function (response) {
            if (response.data) {
                getData();
                $scope.utakmice[$scope.trenutnaUtakmica.id - 1].pobjednik = 4;
            }
        });
    }

    $scope.promijeniKolo = function (kolo) {
        $scope.aktivnoKolo = kolo;
        $scope.utakmiceKola = [];
        for (var id in $scope.utakmice)
            if ($scope.utakmice[id].kolo == kolo)
                $scope.utakmiceKola.push($scope.utakmice[id]);
    }
}]);
