﻿vaterpoloLiga.controller('postavkeController', ['$scope', 'timoviService', 'tipoviService', 'igraciService', function ($scope, timoviService, tipoviService, igraciService) {

    $scope.timovi = null;
    $scope.aktivniTim = null;
    $scope.igraciAktivnogTima = null;
    $scope.igraciMessage = "";

    $scope.tipovi = null;
    $scope.postavkeMessage = "";

    function getData() {
        timoviService.getTimovi().then(function (result) {
            $scope.timovi = result.data;
        });

        tipoviService.getTipovi().then(function (result) {
            $scope.tipovi = result.data;
        });
    }
    getData();

    $scope.promijeniTim = function (tim) {
        igraciService.getIgraciFromTeam(tim.id).then(function (result) {
            $scope.igraciAktivnogTima = result.data;
            $scope.aktivniTim = tim;
        });
    }

    $scope.spremiIgrace = function () {
        igraciService.saveIgraci($scope.igraciAktivnogTima).then(function () {
            $scope.igraciMessage = "Uspješno spremljeno!";
        });
    }

    $scope.spremiPostavke = function () {
        tipoviService.saveTipovi($scope.tipovi).then(function () {
            $scope.postavkeMessage = "Uspješno spremljeno!";
        });
    }
}]);