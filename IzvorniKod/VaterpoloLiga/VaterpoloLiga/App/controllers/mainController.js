﻿vaterpoloLiga.controller('mainController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {

    $scope.authentication = authService.authentication;

    $scope.loginData = {
        userName: "",
        password: ""
    }

    $scope.message = "";

    $scope.login = function (userName, password) {

        authService.login($scope.loginData).then(function (response) {
            if (response.ovlastID == 4)
                $location.path('profile');
        },
        function (err) {
            $scope.message = err.error_description;
        });
    }

    $scope.logout = function () {

        authService.logOut();
        $location.path('#/index');
    }
}]);