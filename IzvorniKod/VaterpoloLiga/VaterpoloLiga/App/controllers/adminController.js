﻿vaterpoloLiga.controller('adminController', ['$scope', 'simulationService', function ($scope, simulationService) {

    $scope.message = "";
    $scope.brojSimulacija = null;

    $scope.simuliraj = function () {
        $scope.message = "";
        simulationService.simuliraj($scope.brojSimulacija).then(function (response) {
            if (response.data == true)
                $scope.message = "Uspješno simulirano!";
        });
    }
}]);