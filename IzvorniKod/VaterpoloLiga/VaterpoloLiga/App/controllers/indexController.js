﻿vaterpoloLiga.controller('indexController', ['$scope', 'tabliceService', function ($scope, tabliceService) {

    $scope.activetab = 1;
    $scope.liga = null;
    $scope.rezultati = null;
    $scope.najkorisnijiIgraci = null;

    $scope.kola = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

    function getData() {
        tabliceService.getLiga().then(function (result) {
            $scope.liga = result.data;
        });

        tabliceService.getRezultati().then(function (result) {
            $scope.rezultati = result.data;
        });

        tabliceService.najkorisnijiIgraci().then(function (result) {
            $scope.najkorisnijiIgraci = result.data;
        })
    }
    getData();

    $scope.promijeniKolo = function (kolo) {
        console.log(kolo);
        tabliceService.getRezultati(kolo).then(function (result) {
            $scope.rezultati = result.data;
            console.log(result.data);
        });
    }

    $scope.play = [
        { placee: '1.', player: 'ImePrezime01' },
        { placee: '2.', player: 'ImePrezime02' },
        { placee: '3.', player: 'ImePrezime03' },
        { placee: '4.', player: 'ImePrezime04' },
        { placee: '5.', player: 'ImePrezime05' },
        { placee: '6.', player: 'ImePrezime06' },
        { placee: '7.', player: 'ImePrezime07' },
        { placee: '8.', player: 'ImePrezime08' },
        { placee: '9.', player: 'ImePrezime09' },
        { placee: '10.', player: 'ImePrezime10' }
    ];
}]);
