﻿vaterpoloLiga.controller('registerController', ['$scope', '$location', '$timeout', 'authService', 'drzaveService', 'timoviService', 'igraciService', 'virtualniTimoviService', function ($scope, $location, $timeout, authService, drzaveService, timoviService, igraciService, virtualniTimoviService) {

    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.drzave = null;
    $scope.timovi = null;
    $scope.igraci = [
        [], [], [], [], [],
    ];

    $scope.loginData = {
        userName: "",
        password: ""
    }

    $scope.registration = {
        ime: "",
        prezime: "",
        userName: "",
        email: "",
        password: "",
        confirmPassword: "",
        drzavaID: "",
        timID: "",
        virtualniTimID: ""
    }

    $scope.virtualniTim = {
        naziv: "",
        vratar: "",
        bek: "",
        centar: "",
        krilo1: "",
        krilo2: "",
        vanjski1: "",
        vanjski2: ""
    }

    function getData() {
        drzaveService.getDrzave().then(function (result) {
            $scope.drzave = result.data;
        });

        timoviService.getTimovi().then(function (result) {
            $scope.timovi = result.data;
        });

        igraciService.getIgraci().then(function (result) {
            for (var igrac in result.data) {
                $scope.igraci[result.data[igrac].pozicijaID - 1].push(result.data[igrac]);
            }
        });
    }
    getData();

    $scope.register = function () {

        virtualniTimoviService.createVirtualniTim($scope.virtualniTim).then(function (data) {
            $scope.registration.virtualniTimID = data.data;

            authService.saveRegistration($scope.registration).then(function (response) {

                $scope.loginData.userName = $scope.registration.userName;
                $scope.loginData.password = $scope.registration.password;
                authService.login($scope.loginData).then(function (response) {
                    //$location.path('#/' + response.NEXTPAGE);
                },
                    function (err) {
                        $scope.message = err.error_description;
                    }
                );

                $scope.savedSuccessfully = true;
                $scope.message = "Uspješno ste registrirani, bit ćete preusmjereni na " + response.data + "stranicu za 5 sekundi.";
                startTimer(response.data);

            }, function (response) {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                    $scope.message = "Failed to register user due to:" + errors.join(' ');
                }
            );
        });
    };

    var startTimer = function (path) {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/' + path);
        }, 5000);
    }
}]);