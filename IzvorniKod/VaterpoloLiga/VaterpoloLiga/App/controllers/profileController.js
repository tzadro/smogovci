﻿vaterpoloLiga.controller('profileController', ['$scope', 'virtualniTimoviService', 'drzaveService', 'timoviService', 'tabliceService', function ($scope, virtualniTimoviService, drzaveService, timoviService, tabliceService) {

    $scope.activetab = 1;
    $scope.virtualniTim = null;
    $scope.drzava = null;
    $scope.tim = null;
    $scope.globalno = null;
    $scope.drzavno = null;
    $scope.timsko = null;

    function getData() {
        virtualniTimoviService.getVirtualniTimById($scope.$parent.authentication.virtualniTimID).then(function (response) {
            $scope.virtualniTim = response.data;
        });

        drzaveService.getDrzavaById($scope.$parent.authentication.drzavaID).then(function (response) {
            $scope.drzava = response.data;
        });

        timoviService.getTimById($scope.$parent.authentication.timID).then(function (response) {
            $scope.tim = response.data;
        });

        tabliceService.getGlobalno().then(function (response) {
            $scope.globalno = response.data;
        })

        tabliceService.getDrzavno($scope.$parent.authentication.drzavaID).then(function (response) {
            $scope.drzavno = response.data;
        })

        tabliceService.getTimsko($scope.$parent.authentication.timID).then(function (response) {
            $scope.timsko = response.data;
        })
    }
    getData();

    $scope.team = [
        { place: '1.', name: 'nwk8' },
        { place: '2.', name: 'wdbjkw7' },
        { place: '3.', name: 'klndknd' },
        { place: '4.', name: 'aaaa' },
        { place: '5.', name: 'cccc' },
        { place: '6.', name: 'jaomeni' },
        { place: '7.', name: 'nklcel' },
        { place: '8.', name: 'brljbrlj' }
    ];
}]);
