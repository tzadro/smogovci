﻿var vaterpoloLiga = angular.module('vaterpoloLiga', ['ngRoute', 'LocalStorageModule']);

vaterpoloLiga.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

vaterpoloLiga.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/index', {
        controller: 'indexController',
        templateUrl: '/App/templates/index.html'
    }).when('/register', {
        controller: 'registerController',
        templateUrl: '/App/templates/register.html'
    }).when('/profile', {
        controller: 'profileController',
        templateUrl: '/App/templates/profile.html'
    }).when('/utakmice', {
        controller: 'utakmiceController',
        templateUrl: '/App/templates/utakmice.html'
    }).when('/postavke', {
        controller: 'postavkeController',
        templateUrl: '/App/templates/postavke.html'
    }).when('/admin', {
        controller: 'adminController',
        templateUrl: '/App/templates/admin.html'
    }).when('/igrac-utakmice', {
        controller: 'igracUtakmiceController',
        templateUrl: '/App/templates/igrac-utakmice.html'
    }).otherwise({
        redirectTo: '/index'
    });
}]);